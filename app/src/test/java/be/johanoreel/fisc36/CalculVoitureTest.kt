package be.johanoreel.fisc36

import be.johanoreel.fisc36.voiture.Calcul
import be.johanoreel.fisc36.voiture.Moteur
import org.hamcrest.CoreMatchers
import org.junit.Test

import org.hamcrest.MatcherAssert.assertThat
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import org.threeten.bp.LocalDate

@RunWith(Parameterized::class)
class CalculVoitureTest(private val exercice: Int,
                        private val valeurCatalogue: Double,
                        private val immatriculation: LocalDate,
                        private val moteur: Moteur,
                        private val emissionDonnee: Int?,
                        private val premierJourDisposition: LocalDate?,
                        private val dernierJourDisposition: LocalDate?,
                        private val montantFinal: Double) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data() = listOf(
            arrayOf( // 1. (page 6)
                2013,
                30000.00,
                LocalDate.of(2011,6,21),
                Moteur.ESSENCE,
                120,
                null,
                null,
                1488.73
            ),
            arrayOf( // 1. (page 6 v2022)
                2021,
                30000.00,
                LocalDate.of(2019,6,21),
                Moteur.ESSENCE,
                120,
                null,
                null,
                1587.98
            ),
            arrayOf( // 4.1 (page 8)
                2013,
                37500.00,
                LocalDate.of(2012,8,30),
                Moteur.ESSENCE,
                127,
                LocalDate.of(2012,9,17),
                null,
                623.71
            ),
            arrayOf( // 4.1 (page 8 v2022)
                2021,
                37500.00,
                LocalDate.of(2020,8,30),
                Moteur.ESSENCE,
                127,
                LocalDate.of(2020,9,17),
                null,
                660.95
            ),
            arrayOf( // 4.2 (page 9)
                2013,
                30000.00,
                LocalDate.of(2011,5,20),
                Moteur.DIESEL,
                112,
                null,
                LocalDate.of(2012,8,8),
                1082.83
            ),
            arrayOf( // 4.2 (page 9 v2022)
                2021,
                30000.00,
                LocalDate.of(2019,5,20),
                Moteur.DIESEL,
                112,
                null,
                LocalDate.of(2020,8,8),
                1142.99
            ),
            arrayOf( // 4.3 (page 11)
                2013,
                25000.00,
                LocalDate.of(2008,10,5),
                Moteur.ESSENCE,
                112,
                LocalDate.of(2012,3,1),
                LocalDate.of(2012,3,31),
                98.36
            ),
            arrayOf( // 4.3 (page 11 v2022)
                2021,
                25000.00,
                LocalDate.of(2016,10,5),
                Moteur.ESSENCE,
                112,
                LocalDate.of(2020,3,1),
                LocalDate.of(2020,3,31),
                111.48
            ),
            arrayOf( // 5.1 (page 12)
                2013,
                30000.00,
                LocalDate.of(2009,10,5),
                Moteur.ESSENCE,
                120,
                null,
                LocalDate.of(2012,7,10),
                708.53
            ),
            arrayOf( // 5.1 (page 12 v2020)
                2021,
                30000.00,
                LocalDate.of(2017,10,5),
                Moteur.ESSENCE,
                120,
                null,
                LocalDate.of(2020,7,10),
                755.77
            ),
            arrayOf( // 5.2 (page 13)
                2013,
                35000.00,
                LocalDate.of(2012,7,5),
                Moteur.DIESEL,
                100,
                LocalDate.of(2012,7,10),
                null,
                860.66
            ),
            arrayOf( // 5.2 (page 13 v2022)
                2021,
                35000.00,
                LocalDate.of(2020,7,5),
                Moteur.DIESEL,
                100,
                LocalDate.of(2020,7,10),
                null,
                918.03
            ),
            arrayOf( // 7.1 (page 15)
                2013,
                20000.00,
                LocalDate.of(2010,5,5),
                Moteur.ESSENCE,
                110,
                LocalDate.of(2012,3,1),
                LocalDate.of(2012,6,1),
                301.64
            ),
            arrayOf( // 7.1 (page 15 v2022)
                2021,
                20000.00,
                LocalDate.of(2018,5,5),
                Moteur.ESSENCE,
                110,
                LocalDate.of(2020,3,1),
                LocalDate.of(2020,6,1),
                341.86 // 341.85 -> mal arrondi vers le bas dans le pdf source
            ),
            arrayOf( // 7.2 (page 16)
                2013,
                40000.00,
                LocalDate.of(2012,5,31),
                Moteur.DIESEL,
                120,
                LocalDate.of(2012,6,1),
                null,
                1603.75
            ),
            arrayOf( // 7.2 (page 16 v2022)
                2021,
                40000.00,
                LocalDate.of(2020,5,31),
                Moteur.DIESEL,
                120,
                LocalDate.of(2020,6,1),
                null,
                1683.93
            )
        )
    }

    @Test
    fun calcul_isCorrect() {
        assertThat(
            Calcul(
                exercice,
                valeurCatalogue,
                immatriculation,
                moteur,
                emissionDonnee,
                premierJourDisposition,
                dernierJourDisposition
                ).montantFinal, CoreMatchers.equalTo(montantFinal))
    }
}
