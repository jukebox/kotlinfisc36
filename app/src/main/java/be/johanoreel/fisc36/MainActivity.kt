package be.johanoreel.fisc36

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.method.LinkMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProvider
import be.johanoreel.fisc36.databinding.ActivityMainBinding
import be.johanoreel.fisc36.view.*
import be.johanoreel.fisc36.voiture.Calcul
import be.johanoreel.fisc36.voiture.Moteur
import be.johanoreel.fisc36.voiture.Periode
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.threetenabp.AndroidThreeTen
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.text.NumberFormat
import kotlin.math.abs


/**
 * The main activity, displays the form
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)

        // Allows to use the java 8 backport for LocalDate, won't be necessary anymore the day
        // support for 25 will be dropped
        AndroidThreeTen.init(this)

        // Sets the locale to French if the user's system locale isn't in French or Dutch
        // Without this, date selection, date format and number format are in the system locale
        LocaleHelper.onAttach(this)
        val localeDateFormatter = LocaleHelper.getLocaleDateFormatter()
        val localeNumberFormatter = LocaleHelper.getLocaleNumberFormatter()
        val localeDecimalSeparator = LocaleHelper.getLocaleDecimalSeparator()
        val exerciceStringArray = resources.getStringArray(R.array.exercice_array)
        val moteurStringArray = resources.getStringArray(R.array.moteurs)

        // Restores the data lost on rotation etc
        restoreState(localeDateFormatter, localeNumberFormatter, moteurStringArray)

        // Creates the form
        createForm(
            localeDateFormatter,
            localeNumberFormatter,
            localeDecimalSeparator,
            exerciceStringArray,
            moteurStringArray)
    }

    //====================================================================================
    // ACTIVITY FUNCTIONS
    //====================================================================================

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_clear_all -> {
                clearAllInput()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    //====================================================================================
    // FORM FUNCTIONS
    //====================================================================================

    private fun restoreState(localeDateFormatter: DateTimeFormatter, localeNumberFormatter: NumberFormat, moteurStringArray: Array<String>){
        // Links the activity to the ViewModel
        val viewModel = ViewModelProvider(this)[VoitureViewModel::class.java]

        // Restores the state values back into the form elements
        viewModel.exercice?.let {
            binding.tietExercice.setText(it.toString())
        }

        viewModel.valeurCatalogue?.let {
            // Doesn't use the number formatter here to avoid thousand separators
            binding.tietValeurCatalogue.setText(
                LocaleHelper.formatWithLocaleDecimalSeparator(it.toString())
            )
        }

        viewModel.immatriculation?.let {
            binding.tietImmatriculation.setText(it.format(localeDateFormatter))
        }

        viewModel.moteur?.let {
            binding.tietMoteur.setText(moteurStringArray[it.ordinal])
        }

        viewModel.emission?.let {
            binding.tietEmission.setText(it.toString())
        }

        viewModel.premierJour?.let {
            binding.tietPremierJour.setText(it.format(localeDateFormatter))
        }

        viewModel.dernierJour?.let {
            binding.tietDernierJour.setText(it.format(localeDateFormatter))
        }

        viewModel.calcul?.let {
            showDetails(it, localeDateFormatter)
        }

        // Track input changes in the form elements
        binding.tietExercice.doAfterTextChanged {
            viewModel.exercice =
                if(it.isNullOrEmpty()) null
                else it.toString().toInt()
        }

        binding.tietValeurCatalogue.doAfterTextChanged {
            viewModel.valeurCatalogue =
                if(it.isNullOrEmpty()) null
                else (localeNumberFormatter.parse(it.toString()) ?: 0).toDouble()
        }

        binding.tietImmatriculation.doAfterTextChanged {
            viewModel.immatriculation =
                if(it.isNullOrEmpty()) null
                else LocalDate.parse(it, localeDateFormatter)
        }

        binding.tietMoteur.doAfterTextChanged {
            viewModel.moteur =
                if(it.isNullOrEmpty()) null
                else binding.tietMoteur.getMoteur(moteurStringArray)
        }

        binding.tietEmission.doAfterTextChanged {
            viewModel.emission =
                if(it.isNullOrEmpty()) null
                else it.toString().toInt()
        }

        binding.tietPremierJour.doAfterTextChanged {
            viewModel.premierJour =
                if(it.isNullOrEmpty()) null
                else LocalDate.parse(it, localeDateFormatter)
        }

        binding.tietDernierJour.doAfterTextChanged {
            viewModel.dernierJour =
                if(it.isNullOrEmpty()) null
                else LocalDate.parse(it, localeDateFormatter)
        }
    }

    private fun createForm(dateFormatter: DateTimeFormatter, numberFormatter: NumberFormat, localeSeparator: Char, exerciceStringArray: Array<String>, moteurStringArray: Array<String>){

        // Input field: Exercice
        // Might be replaced by the ExposedDropdownMenu in the 1.1.0 Material Design release
        binding.tietExercice.toSpinner(
            R.drawable.ic_event_note_black_24dp,
            exerciceStringArray) {}

        // Input field: Valeur catalogue
        binding.tietValeurCatalogue.toClearableTextInput(R.drawable.ic_euro_symbol_black_24dp)
        // Only allow numbers with two decimals at the end with the locale separator
        binding.tietValeurCatalogue.filters = arrayOf<InputFilter>(
            DecimalInputFilter(localeSeparator, 9, 2))

        // Input field: Moteur
        // Might be replaced by the ExposedDropdownMenu in the 1.1.0 Material Design release
        binding.tietMoteur.toSpinner(
            R.drawable.ic_local_gas_station_black_24dp,
            moteurStringArray) {

            // Mettre emission CO2 à zéro si le moteur électrique est choisi
            if(binding.tietMoteur.getMoteur(moteurStringArray) == Moteur.ELECTRIQUE) {
                binding.tietEmission.setText("0")
            }
        }

        // Input field: Emission
        binding.tietEmission.toClearableTextInput(R.drawable.ic_cloud_queue_black_24dp)
        // Makes sure someone can't put an infinitely long number
        binding.tietEmission.filters = arrayOf<InputFilter>(
            DecimalInputFilter(localeSeparator,9, 0))

        // Input field: Immatriculation
        binding.tietImmatriculation.toClearableDateInput(R.drawable.ic_event_black_24dp) {
            // Si l'exercice n'est pas choisis, show the standard date picker
            // Sinon limite le calendrier
            if(binding.tietExercice.text.isEmpty()){
                showStandardDatePickerDialog(binding.tilImmatriculation, dateFormatter)
            }
            else {
                val anneeCalendrier = binding.tietExercice.text.toString().toInt() - 1
                showDatePickerDialog(
                    binding.tilImmatriculation,
                    LocalDate.of(anneeCalendrier, 1, 1),
                    null,
                    LocalDate.of(anneeCalendrier, 12, 31),
                    true,
                    dateFormatter)
            }
        }

        // Input field: Premier jour
        binding.tietPremierJour.toClearableDateInput(R.drawable.ic_date_range_black_24dp) {
            // Si l'exercice n'est pas choisis, show the standard date picker
            // Sinon limite le calendrier
            if(binding.tietExercice.text.isEmpty()){
                showStandardDatePickerDialog(binding.tilPremierJour, dateFormatter)
            }
            else {
                val anneeCalendrier = binding.tietExercice.text.toString().toInt() - 1
                showDatePickerDialog(
                    binding.tilPremierJour,
                    LocalDate.of(anneeCalendrier, 1, 1),
                    LocalDate.of(anneeCalendrier, 1, 1),
                    LocalDate.of(anneeCalendrier, 12, 31),
                    false,
                    dateFormatter)
            }
        }

        // Input field: Dernier jour
        binding.tietDernierJour.toClearableDateInput(R.drawable.ic_date_range_black_24dp) {
            // Si l'exercice n'est pas choisis, show the standard date picker
            // Sinon limite le calendrier
            if(binding.tietExercice.text.isEmpty()){
                showStandardDatePickerDialog(binding.tilDernierJour, dateFormatter)
            }
            else {
                val anneeCalendrier = binding.tietExercice.text.toString().toInt() - 1
                showDatePickerDialog(
                    binding.tilDernierJour,
                    LocalDate.of(anneeCalendrier, 12, 31),
                    LocalDate.of(anneeCalendrier, 1, 1),
                    LocalDate.of(anneeCalendrier, 12, 31),
                    false,
                    dateFormatter)
            }
        }

        // Button: Calculer
        binding.btnCalculer.setOnClickListener {

            // If the input is valid
            if (validateInput(dateFormatter)){

                // Clear all potential error messages resulting from previous validations
                clearAllErrorMessages()

                // Gets all the input
                val exercice: Int = binding.tietExercice.text.toString().toInt()
                val valeurCatalogue: Double =
                    (numberFormatter.parse(binding.tietValeurCatalogue.text.toString()) ?: 0).toDouble()
                val immatriculation: LocalDate =
                    LocalDate.parse(binding.tietImmatriculation.text, dateFormatter)
                val moteur: Moteur = binding.tietMoteur.getMoteur(moteurStringArray)
                val emission: Int? =
                    if (binding.tietEmission.text.isNullOrEmpty()) null
                    else binding.tietEmission.text.toString().toInt()
                val premierJour: LocalDate? =
                    if (binding.tietPremierJour.text.isNullOrEmpty()) null
                    else LocalDate.parse(binding.tietPremierJour.text, dateFormatter)
                val dernierJour: LocalDate? =
                    if (binding.tietDernierJour.text.isNullOrEmpty()) null
                    else LocalDate.parse(binding.tietDernierJour.text, dateFormatter)

                // Passes the input on to be processed
                val calcul = Calcul(
                    exercice, valeurCatalogue, immatriculation, moteur, emission,
                    premierJour, dernierJour)

                persistCalcul(calcul)
                // Shows the details of the process
                showDetails(calcul, dateFormatter)
            }
            else {
                // Scroll up to the top if the input isn't valid
                binding.svForm.scrollToTop()
            }

        }

        // Bouton Partager
        binding.btnPartager.setOnClickListener {

            var detail = ""
            binding.llDetail.children.iterator().forEach { child ->
                if(child is TextView && child !is MaterialButton && child.isVisible){
                    detail = detail.plus(child.text.toString()).plus("\n\n")
                }
            }

            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, detail)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }

        // Make links clickable
        binding.tvFAQSPFF.movementMethod = LinkMovementMethod.getInstance()
        binding.tvJohanOreel.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun showDetails(calcul: Calcul, dateFormatter: DateTimeFormatter) {

        binding.tvResultat.text = getString(R.string.resultat, calcul.montantFinal)

        // Données de base

        binding.tvExercice.text = getString(R.string.detail_exercice, calcul.exercice)

        binding.tvValeurCatalogue.text = getString(R.string.detail_valeur_catalogue,
            calcul.valeurCatalogue)

        binding.tvImmatriculation.text = getString(R.string.detail_immatriculation,
            calcul.immatriculation.format(dateFormatter))

        binding.tvImmatriculationCalcul.setTextAndVisibility(
            getString(R.string.detail_immatriculation_calcul,
                calcul.immatricalutionPremierDuMois.format(dateFormatter)
            ),
            calcul.immatriculation.dayOfMonth > 1
        )

        val dropDownMoteur: String = when (calcul.moteur) {
            Moteur.DIESEL -> getString(R.string.dropdown_moteur_diesel)
            Moteur.ESSENCE -> getString(R.string.dropdown_moteur_essence)
            Moteur.LPG -> getString(R.string.dropdown_moteur_lpg)
            Moteur.GAZ_NATUREL -> getString(R.string.dropdown_moteur_gaz_naturel)
            Moteur.ELECTRIQUE -> getString(R.string.dropdown_moteur_electrique)
        }

        binding.tvMoteur.text = getString(R.string.detail_moteur, dropDownMoteur)

        binding.tvEmission.text = getString(R.string.detail_emission,
            calcul.emission,
            if(calcul.emission == calcul.emissionDefaut)
                getString(R.string.detail_emission_valeur_defaut)
            else ""
        )

        binding.tvPremierJour.text = getString(R.string.detail_premier_jour,
            calcul.premierJourCalcul.format(dateFormatter))

        binding.tvDernierJour.text = getString(R.string.detail_dernier_jour,
            if (calcul.dernierJourDisposition != null)
                calcul.dernierJourDisposition.format(dateFormatter)
            else calcul.dernierJourCalcul.format(dateFormatter)
        )

        binding.tvDernierJourCalcul.setTextAndVisibility(
            getString(
                R.string.detail_dernier_jour_calcul,
                calcul.dernierJourCalcul.format(dateFormatter)
            ),
            calcul.dernierJourDisposition != null
        )

        // Pourcentage valeur catalogue

        val pctValeurCataloguePeriode1 = calcul.periode1.pctValeurCatalogue * 100
        val pctValeurCataloguePeriode2 = calcul.periode2.pctValeurCatalogue * 100

        // Periode 1
        binding.tvPctValeurCataloguePeriode1.setTextAndVisibility(
            getString(R.string.detail_pourcentage_text,
                calcul.periode1.premierJour?.format(dateFormatter),
                calcul.periode1.dernierJour?.format(dateFormatter),
                pctValeurCataloguePeriode1
            ),
            calcul.periode1.jours > 0
        )

        // Same as above
        binding.tvPctValeurCataloguePeriode2.setTextAndVisibility(
            getString(R.string.detail_pourcentage_text,
                calcul.periode2.premierJour?.format(dateFormatter),
                calcul.periode2.dernierJour?.format(dateFormatter),
                pctValeurCataloguePeriode2
            ),
            calcul.periode2.jours > 0
        )

        // Emission

        val supInf: String =
            if(calcul.emission < calcul.emissionReference) getString(R.string.detail_inferieur)
            else  getString(R.string.detail_superieur)

        val plusMoins: String =
            if(calcul.emission < calcul.emissionReference) getString(R.string.detail_moins)
            else getString(R.string.detail_plus)

        val emissionCo2ParDefaut =
            if (calcul.emission == calcul.emissionDefaut)
                getString(R.string.detail_emissions_par_defaut).plus(" ")
            else ""

        val detailMoteur: String = when (calcul.moteur) {
            Moteur.DIESEL -> getString(R.string.detail_moteur_diesel)
            Moteur.ESSENCE -> getString(R.string.detail_moteur_essence)
            Moteur.LPG -> getString(R.string.detail_moteur_lpg)
            Moteur.GAZ_NATUREL -> getString(R.string.detail_moteur_gaz_naturel)
            Moteur.ELECTRIQUE -> getString(R.string.detail_moteur_electrique)
        }

        binding.tvEmissionIntro.setTextAndVisibility(
            getString(R.string.detail_emissions_intro,
                emissionCo2ParDefaut,
                calcul.emission,
                abs(calcul.emissionMoinsReference),
                supInf,
                calcul.emissionReference,
                detailMoteur
            ),
            calcul.moteur != Moteur.ELECTRIQUE
        )

        binding.tvEmissionCalcul.setTextAndVisibility(
            getString(R.string.detail_emissions_calcul,
                Calcul.PCT_EMISSION_BASE * 100,
                plusMoins,
                Calcul.PCT_EMISSION_MOD * 100,
                abs(calcul.emissionMoinsReference),
                calcul.pctEmissionTheorique * 100
            ),
            calcul.moteur != Moteur.ELECTRIQUE
        )

        binding.tvEmissionElectrique.setTextAndVisibility(
            getString(
                R.string.detail_emissions_electrique,
                Calcul.PCT_EMISSION_MIN * 100
            ),
            calcul.moteur == Moteur.ELECTRIQUE
        )

        val pctEmissionFinal = calcul.pctEmissionFinal * 100

        // Max
        binding.tvEmissionMax.setTextAndVisibility(
            getString(R.string.detail_emissions_calcul_max,
                pctEmissionFinal
            ),
            calcul.pctEmissionTheorique > Calcul.PCT_EMISSION_MAX
        )

        // Min
        binding.tvEmissionMin.setTextAndVisibility(
            getString(R.string.detail_emissions_calcul_min,
                pctEmissionFinal
            ),
            calcul.pctEmissionTheorique < Calcul.PCT_EMISSION_MIN
        )

        // Jours

        binding.tvJoursPeriode1.setTextAndVisibility(
            getString(R.string.detail_nombre_jours_text,
                calcul.periode1.premierJour?.format(dateFormatter),
                calcul.periode1.dernierJour?.format(dateFormatter),
                calcul.periode1.jours
            ),
            calcul.periode1.jours > 0
        )

        binding.tvJoursPeriode2.setTextAndVisibility(
            getString(R.string.detail_nombre_jours_text,
                calcul.periode2.premierJour?.format(dateFormatter),
                calcul.periode2.dernierJour?.format(dateFormatter),
                calcul.periode2.jours
            ),
            calcul.periode2.jours > 0
        )

        binding.tvJoursCalendrier.text = getString(R.string.detail_nombre_jours_annee,
            calcul.anneeRevenus,
            calcul.joursCalendrier
        )

        // Calcul montants

        binding.tvMontantPeriode1.setTextAndVisibility(
            getString(R.string.detail_calcul_text,
                calcul.periode1.premierJour?.format(dateFormatter),
                calcul.periode1.dernierJour?.format(dateFormatter),
                calcul.valeurCatalogue,
                pctValeurCataloguePeriode1,
                Periode.NUMERATEUR,
                Periode.DENOMINATEUR,
                calcul.periode1.jours,
                calcul.joursCalendrier,
                pctEmissionFinal,
                calcul.periode1.montant
            ),
            calcul.periode1.jours > 0
        )

        binding.tvMontantPeriode2.setTextAndVisibility(
            getString(R.string.detail_calcul_text,
                calcul.periode2.premierJour?.format(dateFormatter),
                calcul.periode2.dernierJour?.format(dateFormatter),
                calcul.valeurCatalogue,
                pctValeurCataloguePeriode2,
                Periode.NUMERATEUR,
                Periode.DENOMINATEUR,
                calcul.periode2.jours,
                calcul.joursCalendrier,
                pctEmissionFinal,
                calcul.periode2.montant
            ),
            calcul.periode2.jours > 0
        )

        binding.tvMontantSomme.setTextAndVisibility(
            getString(R.string.detail_calcul_sum,
                calcul.anneeRevenus,
                calcul.periode1.montant,
                calcul.periode2.montant,
                calcul.montantTotal
            ),
            calcul.periode1.jours > 0 && calcul.periode2.jours > 0
        )

        val sommeJours =
            if(calcul.periode1.jours > 0 && calcul.periode2.jours > 0)
                getString(R.string.detail_somme_jours,
                    calcul.periode1.jours,
                    calcul.periode2.jours)
            else (calcul.joursTotal).toString()

        binding.tvMontantMinimum.setTextAndVisibility(
            getString(R.string.detail_calcul_min,
                calcul.anneeRevenus,
                calcul.montantMinimumTheorique,
                sommeJours,
                calcul.joursCalendrier,
                calcul.montantMinimum
            ),
            calcul.montantTotal < calcul.montantMinimum
        )

        // Scroll down the form when the visibility changes
        binding.llDetail.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                binding.svForm.smoothScrollTo(0, binding.btnCalculer.top)
                binding.llDetail.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })

        // Make detail part visible
        binding.llDetail.visibility = View.VISIBLE
    }

    private fun ScrollView.scrollToTop(){
        this.fullScroll(ScrollView.FOCUS_UP)
    }

    private fun TextView.setTextAndVisibility(text: String, shouldBeVisible: Boolean) {
        this.text = text
        this.visibility =
            if(shouldBeVisible) View.VISIBLE
            else View.GONE
    }

    //====================================================================================
    // HELPER FUNCTIONS
    //====================================================================================

    private fun clearAllInput(){

        // Scroll up the form when the visibility changes
        binding.llDetail.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                binding.svForm.scrollToTop()
                binding.llDetail.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
        binding.llDetail.visibility = View.GONE
        clearCalcul()

        clearAllErrorMessages { til ->
            (til.editText as EditText).text.clear()
        }
        binding.svForm.scrollToTop()
    }

    private fun clearCalcul() {
        persistCalcul(null)
    }

    private fun persistCalcul(calcul: Calcul?){
        ViewModelProvider(this)[VoitureViewModel::class.java].calcul = calcul
    }

    private fun clearAllErrorMessages(){
        clearAllErrorMessages {  }
    }

    private fun clearAllErrorMessages(afterClearErrorMessages: (til: TextInputLayout) -> Unit){
        hideKeyboard()
        binding.llForm.children.iterator().forEach { child ->
            if(child is TextInputLayout){
                child.clearErrorMessage()
                afterClearErrorMessages(child)
            }
        }
    }

    //https://stackoverflow.com/questions/1109022/how-to-set-visibility-android-soft-keyboard
    private fun hideKeyboard() {
        val imm =
            this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = this.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
        view.clearFocus()
        binding.llFocusHolder.requestFocus()
    }

    private fun showDatePickerDialog(
        parent: TextInputLayout,
        defaultDate: LocalDate?,
        minDate: LocalDate?,
        maxDate: LocalDate,
        showYearPickerFirst: Boolean,
        dateFormatter: DateTimeFormatter) {

        val newFragment = DatePickerFragment(
            parent,
            defaultDate,
            minDate,
            maxDate,
            showYearPickerFirst,
            dateFormatter
        )
        newFragment.show(supportFragmentManager, "datePicker")
    }

    private fun showStandardDatePickerDialog(parent: TextInputLayout, dateFormatter: DateTimeFormatter){
        val today = LocalDate.now()
        showDatePickerDialog(parent, today, null, today, true, dateFormatter)
    }

    //====================================================================================
    // LOCAL EXTENSION METHODS
    //====================================================================================

    private fun TextInputEditText.toClearableTextInput(leftDrawableId: Int) {
        this.toClearableTextInput(
            leftDrawableId,
            R.drawable.ic_cancel_black_24dp,
            ::hideKeyboard)
    }

    private fun TextInputEditText.toClearableDateInput(
        leftDrawable: Int,
        onShowDatePicker: () -> Unit) {

        this.toClearableDateInput(
            leftDrawable,
            R.drawable.ic_cancel_black_24dp,
            ::hideKeyboard,
            onShowDatePicker)
    }

    private fun AutoCompleteTextView.toSpinner(
        leftDrawable: Int,
        data: Array<String>,
        afterItemSelected: () -> Unit){
        this.toSpinner(
            leftDrawable,
            R.drawable.ic_arrow_drop_down_black_24dp,
            R.drawable.ic_arrow_drop_up_black_24dp,
            data,
            ArrayAdapter(this.context, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item, data),
            ::hideKeyboard,
            afterItemSelected)
    }

    private fun EditText.getMoteur(moteurStringArray: Array<String>) : Moteur {
        return Moteur.entries[moteurStringArray.indexOf(this.text.toString())]
    }

    //====================================================================================
    // VALIDATION FUNCTIONS
    //====================================================================================

    private fun TextInputLayout.validateMandatoryInput(errorMessageId: Int) : Boolean {
        val isFilled : Boolean = (this.editText as EditText).text.isNotEmpty()
        if(!isFilled){
            this.setErrorMessage(getString(errorMessageId))
        }
        return isFilled
    }

    private fun TextInputLayout.validateInputAfterDate(ld: LocalDate, errorMessageId: Int, dateFormatter: DateTimeFormatter) : Boolean {
        val isAfter = (this.editText as EditText).getDate(dateFormatter).isAfter(ld)
        if(isAfter){
            this.setErrorMessage(getString(errorMessageId))
        }
        return !isAfter
    }

    private fun TextInputLayout.validateInputInYear(year: Int, errorMessageId: Int, dateFormatter: DateTimeFormatter) : Boolean {
        val isInYear = (this.editText as EditText).getDate(dateFormatter).year == year
        if (!isInYear){
            this.setErrorMessage(getString(errorMessageId))
        }
        return isInYear
    }

    private fun EditText.getDate(dateFormatter: DateTimeFormatter): LocalDate {
        return LocalDate.parse(this.text, dateFormatter)
    }

    private fun validateInput(dateFormatter: DateTimeFormatter) : Boolean {

        var isValidInput = true
        // Validations pour l'exercice d'imposition
        isValidInput = binding.tilExercice.validateMandatoryInput(R.string.erreur_exercice_vide)
                && isValidInput
        // Validation pour le moteur
        isValidInput = binding.tilMoteur.validateMandatoryInput(R.string.erreur_moteur_vide)
                && isValidInput
        // Validation pour la valeur catalogue
        isValidInput = binding.tilValeurCatalogue.validateMandatoryInput(R.string.erreur_valeur_catalogue_vide)
                && isValidInput

        // Validation pour la date de première immatriculation
        val isImmatriculationFilled =
            binding.tilImmatriculation.validateMandatoryInput(R.string.erreur_immatriculation_vide)
        isValidInput = isImmatriculationFilled && isValidInput

        // Date validation
        if (!binding.tietExercice.text.isNullOrEmpty()){

            var isValidDateInput = true
            val anneeCalendrier = binding.tietExercice.text.toString().toInt() - 1
            val premierJourFilled = !binding.tietPremierJour.text.isNullOrEmpty()
            val dernierJourFilled = !binding.tietDernierJour.text.isNullOrEmpty()

            // Immatriculation
            if (isImmatriculationFilled) {

                if(premierJourFilled){
                    // if immatriculation after first day when a first day is selected
                    isValidDateInput = binding.tilImmatriculation.validateInputAfterDate(
                        binding.tietPremierJour.getDate(dateFormatter),
                        R.string.erreur_immatriculation_apres_premier_jour_disposition,
                        dateFormatter
                    )
                }
                else {
                    // if after first calendar day when no first and last days are selected
                    isValidDateInput = binding.tilImmatriculation.validateInputAfterDate(
                        LocalDate.of(anneeCalendrier, 1, 1), // Premier jour calendrier
                        R.string.erreur_immatriculation_apres_premier_jour_calendrier,
                        dateFormatter
                    )
                }
            }

            // Premier jour
            if(premierJourFilled){

                // Après le dernier jour
                if(dernierJourFilled){
                    isValidDateInput = binding.tilPremierJour.validateInputAfterDate(
                        binding.tietDernierJour.getDate(dateFormatter),
                        R.string.erreur_premier_jour_apres_dernier,
                        dateFormatter
                    ) && isValidDateInput
                }

                // Dans l'année calendrier
                isValidDateInput = binding.tilPremierJour.validateInputInYear(
                    anneeCalendrier,
                    R.string.erreur_premier_jour_hors_annee_calendrier,
                    dateFormatter
                ) && isValidDateInput
            }

            // Dernier jour
            if(dernierJourFilled){

                // Dans l'année calendrier
                isValidDateInput = binding.tilDernierJour.validateInputInYear(
                    anneeCalendrier,
                    R.string.erreur_dernier_jour_hors_annee_calendrier,
                    dateFormatter
                ) && isValidDateInput
            }

            isValidInput = isValidDateInput && isValidInput
        }

        return isValidInput
    }

    //=====================================================================================
}