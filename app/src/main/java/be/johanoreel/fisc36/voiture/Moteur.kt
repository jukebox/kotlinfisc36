package be.johanoreel.fisc36.voiture

enum class Moteur {
    DIESEL, ESSENCE, LPG, GAZ_NATUREL, ELECTRIQUE
}