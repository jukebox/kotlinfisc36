package be.johanoreel.fisc36.voiture

import org.threeten.bp.LocalDate
import org.threeten.bp.temporal.ChronoUnit
import kotlin.math.roundToInt

class Periode (
    val premierJour: LocalDate?,
    val dernierJour: LocalDate?,
    modificateur: Int,
    valeurCatalogue: Double,
    joursCalendrier: Int,
    pctEmission: Double) {

    companion object {
        const val PCT_VALEUR_CATALOGUE_MOD = 0.06
        const val PCT_VALEUR_CATALOGUE_MIN = 0.7
        const val NUMERATEUR = 6
        const val DENOMINATEUR = 7
    }

    val jours =
        if (premierJour == null || dernierJour == null) 0
        else ChronoUnit.DAYS.between(premierJour, dernierJour).toInt() + 1

    private val pctValeurCatalogueTheorique: Double =  1 - PCT_VALEUR_CATALOGUE_MOD * modificateur
    val pctValeurCatalogue: Double =
        if (pctValeurCatalogueTheorique < PCT_VALEUR_CATALOGUE_MIN) PCT_VALEUR_CATALOGUE_MIN
        else pctValeurCatalogueTheorique

    val montant: Double = (100.0 * valeurCatalogue * pctValeurCatalogue * pctEmission *
            jours / joursCalendrier * NUMERATEUR / DENOMINATEUR).roundToInt() / 100.0
}


