package be.johanoreel.fisc36.voiture

import org.threeten.bp.LocalDate
import org.threeten.bp.Year
import kotlin.math.roundToInt

class Calcul(
    val exercice: Int,
    val valeurCatalogue: Double,
    val immatriculation: LocalDate,
    val moteur: Moteur,
    emissionDonnee: Int?,
    premierJourDisposition: LocalDate?,
    val dernierJourDisposition: LocalDate?) {

    companion object {
        const val PCT_EMISSION_BASE = 0.055
        const val PCT_EMISSION_MOD = 0.001
        const val PCT_EMISSION_MIN = 0.04
        const val PCT_EMISSION_MAX = 0.18

        private const val EMISSION_DEFAUT_DIESEL = 195
        @JvmField val EMISSION_REFERENCES_DIESEL = mapOf(
            2012 to 95,
            2013 to 95,
            2014 to 93,
            2015 to 91,
            2016 to 89,
            2017 to 87,
            2018 to 86,
            2019 to 88,
            2020 to 91,
            2021 to 84,
            2022 to 75,
            2023 to 67
        )

        private const val EMISSION_DEFAUT_ESSENCE = 205
        @JvmField val EMISSION_REFERENCES_ESSENCE = mapOf(
            2012 to 115,
            2013 to 116,
            2014 to 112,
            2015 to 110,
            2016 to 107,
            2017 to 105,
            2018 to 105,
            2019 to 107,
            2020 to 111,
            2021 to 102,
            2022 to 91,
            2023 to 82
        )

        private const val EMISSION_DEFAUT_ELECTRIQUE = 0
        @JvmField val EMISSION_REFERENCES_ELECTRIQUE = mapOf(
            2012 to 0,
            2013 to 0,
            2014 to 0,
            2015 to 0,
            2016 to 0,
            2017 to 0,
            2018 to 0,
            2019 to 0,
            2020 to 0,
            2021 to 0,
            2022 to 0,
            2023 to 0
        )

        @JvmField val EMISSION_DEFAUT = mapOf(
            Moteur.DIESEL to EMISSION_DEFAUT_DIESEL,
            Moteur.ESSENCE to EMISSION_DEFAUT_ESSENCE,
            Moteur.LPG to EMISSION_DEFAUT_ESSENCE,
            Moteur.GAZ_NATUREL to EMISSION_DEFAUT_ESSENCE,
            Moteur.ELECTRIQUE to EMISSION_DEFAUT_ELECTRIQUE
        )

        @JvmField val EMISSION_REFERENCES = mapOf(
            Moteur.DIESEL to EMISSION_REFERENCES_DIESEL,
            Moteur.ESSENCE to EMISSION_REFERENCES_ESSENCE,
            Moteur.LPG to EMISSION_REFERENCES_ESSENCE,
            Moteur.GAZ_NATUREL to EMISSION_REFERENCES_ESSENCE,
            Moteur.ELECTRIQUE to EMISSION_REFERENCES_ELECTRIQUE
        )

        @JvmField val MONTANTS_MINIMUMS = mapOf(
            2012 to 1200,
            2013 to 1230,
            2014 to 1250,
            2015 to 1250,
            2016 to 1260,
            2017 to 1280,
            2018 to 1310,
            2019 to 1340,
            2020 to 1360,
            2021 to 1370,
            2022 to 1400,
            2023 to 1540
        )
    }

    val immatricalutionPremierDuMois: LocalDate =
        LocalDate.of(immatriculation.year, immatriculation.monthValue, 1)

    // Si emission de CO2 n'est pas spécifié, prendre valeur par défaut
    val emissionDefaut = EMISSION_DEFAUT[moteur]!!

    val emission = emissionDonnee ?: emissionDefaut

    // L'année calendrier est l'année précédant l'année d'exercice
    val anneeRevenus: Int = exercice - 1

    // Si le premier jour n'est pas spécifié, prendre le premier jour de l'année calendrier
    val premierJourCalcul: LocalDate =
        premierJourDisposition ?: LocalDate.of(anneeRevenus, 1, 1)

    // Si le dernier jour n'est pas spécifié, prendre le premier jour de l'année d'exercice
    val dernierJourCalcul: LocalDate =
        (dernierJourDisposition ?: LocalDate.of(exercice, 1, 1))
            .minusDays(1) // Retirer le dernier jour du calcul (01/01/2013 -> 31/12/2012)

    // Determine le jour pivot qui coupe la période calculé en deux
    private val jourPivot = LocalDate.of(anneeRevenus, immatriculation.monthValue, 1)

    // Determine les dates qui délimitent les deux périodes
    private val premierJourPeriode1  =
        if (premierJourCalcul.isBefore(jourPivot)) premierJourCalcul
        else null

    private val dernierJourPeriode1 = when {
        dernierJourCalcul.isBefore(jourPivot) -> dernierJourCalcul
        premierJourCalcul.isBefore(jourPivot) -> jourPivot.minusDays(1)
        else -> null
    }

    private val premierJourPeriode2 = when {
        jourPivot.isBefore(premierJourCalcul) -> premierJourCalcul
        dernierJourCalcul.isAfter(jourPivot) -> jourPivot
        else -> null
    }

    private val dernierJourPeriode2 =
        if (dernierJourCalcul.isAfter(jourPivot)) dernierJourCalcul
        else null

    // Modificateurs pour le calcul du pourcentage des périodes
    private val modPeriode2 = anneeRevenus - immatriculation.year
    private val modPeriode1 = modPeriode2 - 1

    // La réference d'emmission de co2 pour l'année calendrier
    val emissionReference = EMISSION_REFERENCES[moteur]!![anneeRevenus]!!

    // Calcul du pourcentage de co2 pour les deux périodes
    val emissionMoinsReference = emission - emissionReference
    val pctEmissionTheorique = PCT_EMISSION_BASE + PCT_EMISSION_MOD * (emissionMoinsReference)

    // electrique ou sous minimum, donne le minimum
    val pctEmissionFinal = when {
        (moteur == Moteur.ELECTRIQUE || pctEmissionTheorique < PCT_EMISSION_MIN) -> PCT_EMISSION_MIN
        pctEmissionTheorique > PCT_EMISSION_MAX -> PCT_EMISSION_MAX // ne peut dépasser le maximum
        else -> pctEmissionTheorique
    }

    // Détermine le nombre de jours dans l'année calendrier
    val joursCalendrier = Year.of(anneeRevenus).length() // ChronoUnit.DAYS.between(premierJourCalendrier, premierJourExercice)

    // Crée les deux périodes
    val periode1 = Periode(
        premierJourPeriode1,
        dernierJourPeriode1,
        modPeriode1,
        valeurCatalogue,
        joursCalendrier,
        pctEmissionFinal
    )

    val periode2 = Periode(
        premierJourPeriode2,
        dernierJourPeriode2,
        modPeriode2,
        valeurCatalogue,
        joursCalendrier,
        pctEmissionFinal
    )

    // Additionne les deux périodes
    val montantTotal = periode1.montant + periode2.montant

    val montantMinimumTheorique = MONTANTS_MINIMUMS.getValue(anneeRevenus).toDouble()
    val joursTotal = periode1.jours + periode2.jours

    // Calcul le montant minimum
    val montantMinimum = (100.0 * montantMinimumTheorique * joursTotal / joursCalendrier).roundToInt() / 100.0

    // Le montant total ne peut être inférieur au montant minimum
    val montantFinal =
        if (montantTotal < montantMinimum) montantMinimum
        else montantTotal
}



