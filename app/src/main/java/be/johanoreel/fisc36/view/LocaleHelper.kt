package be.johanoreel.fisc36.view

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

//https://stackoverflow.com/questions/52718347/android-defining-the-default-locale
object LocaleHelper {

    @JvmField val SUPPORTED_LOCALES = arrayOf("fr","nl")
    private const val DEFAULT_LOCALE = "fr"

    fun onAttach(context: Activity): Context {
        val lang: String = Locale.getDefault().language
        return if (SUPPORTED_LOCALES.contains(lang)) context
            else setDefaultLocale(context)
    }

    private fun setDefaultLocale(context: Context): Context {
        val locale = Locale(DEFAULT_LOCALE)
        Locale.setDefault(locale)
        return updateResources(context, locale)
    }

    private fun updateResources(context: Context, locale: Locale): Context {
        val configuration: Configuration = context.resources.configuration
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        return context.createConfigurationContext(configuration)
    }

    fun getLocaleNumberFormatter(): NumberFormat {
        //https://stackoverflow.com/questions/10411414/how-to-format-double-value-for-a-given-locale-and-number-of-decimal-places
        val formatLocale: Locale = Locale.getDefault(Locale.Category.FORMAT)
        val numberFormat: NumberFormat = NumberFormat.getInstance(formatLocale)
        numberFormat.maximumFractionDigits = 2
        numberFormat.minimumFractionDigits = 0
        return numberFormat
    }

    fun getLocaleDateFormatter(): DateTimeFormatter {
        return DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
    }

    fun getLocaleDecimalSeparator(): Char {
        return DecimalFormatSymbols.getInstance().decimalSeparator
    }

    fun formatWithLocaleDecimalSeparator(s: String): String {
        return s.replace('.', getLocaleDecimalSeparator())
    }
}