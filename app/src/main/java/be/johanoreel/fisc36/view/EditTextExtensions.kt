package be.johanoreel.fisc36.view

import android.annotation.SuppressLint
import android.text.InputFilter
import android.text.Spanned
import android.view.MotionEvent
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Pattern

private const val COMPOUND_DRAWABLE_LEFT_INDEX = 0
private const val COMPOUND_DRAWABLE_RIGHT_INDEX = 2

private fun EditText.setLeftRightDrawables(leftDrawableId: Int, rightDrawable: Int) {
    this.setCompoundDrawablesWithIntrinsicBounds(
        leftDrawableId, 0,
        rightDrawable, 0)
}

//https://medium.com/@dimabatyuk/adding-clear-button-to-edittext-9655e9dbb721
private fun EditText.toClearableInput(
    leftDrawable: Int,
    rightDrawable: Int) : () -> Unit  {

    this.setLeftRightDrawables(leftDrawable, rightDrawable)

    val clearDrawable = compoundDrawables[COMPOUND_DRAWABLE_RIGHT_INDEX]
    val updateRightDrawable = {
        this.setCompoundDrawablesWithIntrinsicBounds(
            compoundDrawables[COMPOUND_DRAWABLE_LEFT_INDEX],
            null,
            if (text.isNotEmpty()) clearDrawable else null,
            null)
    }
    updateRightDrawable()

    this.doAfterTextChanged {
        updateRightDrawable()
    }

    return updateRightDrawable
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.toClearableTextInput(
    leftDrawable: Int,
    rightDrawable: Int,
    hideKeyboard: () -> Unit) {

    val parent = this.getTextInputLayout()
    val updateRightDrawable = this.toClearableInput(leftDrawable, rightDrawable)

    //https://medium.com/@dimabatyuk/adding-clear-button-to-edittext-9655e9dbb721
    this.setOnTouchListener { v, event ->
        var hasConsumed = false
        if (v is EditText) {
            parent.clearErrorMessage()
            if (event.x >= v.width - v.totalPaddingRight) {
                if (event.action == MotionEvent.ACTION_UP) {
                    this.text.clear()
                    updateRightDrawable()

                    if(this.hasFocus()){
                        hideKeyboard()
                    }
                }
                hasConsumed = true
            }
        }
        hasConsumed
    }

    //https://stackoverflow.com/questions/5077425/android-detect-done-key-press-for-onscreen-keyboard
    this.setOnEditorActionListener { _, actionId, _ ->
        if(actionId == EditorInfo.IME_ACTION_DONE){
            hideKeyboard()
            true
        } else {
            false
        }
    }
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.toClearableDateInput(
    leftDrawable: Int,
    rightDrawable: Int,
    hideKeyboard: () -> Unit,
    onShowDatePicker: () -> Unit) {

    val parent = this.getTextInputLayout()
    val updateRightDrawable = this.toClearableInput(leftDrawable, rightDrawable)

    //https://stackoverflow.com/questions/3554377/handling-click-events-on-a-drawable-within-an-edittext
    this.setOnTouchListener { v, event ->
        hideKeyboard()
        if(v is EditText){
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.x >= v.width - v.totalPaddingRight) {
                    this.text.clear()
                    updateRightDrawable()
                    parent.clearErrorMessage()
                }
                else{
                    onShowDatePicker()
                }
            }
        }
        false
    }
}

//https://stackoverflow.com/questions/49320728/howto-autocompletetextview-as-spinner-replacement-but-inside-focus-flow
@SuppressLint("ClickableViewAccessibility")
fun AutoCompleteTextView.toSpinner(
    leftDrawable: Int,
    downDrawable: Int,
    upDrawable: Int,
    data: Array<String>,
    adapter: ArrayAdapter<String>,
    hideKeyboard: () -> Unit,
    afterItemSelected: () -> Unit) {

    val parent = this.getTextInputLayout()

    val showArrowDown = {
        this.setLeftRightDrawables(leftDrawable, downDrawable)
    }
    val showArrowUp = {
        this.setLeftRightDrawables(leftDrawable, upDrawable)
    }

    this.setOnTouchListener { view, _ ->
        hideKeyboard()
        showArrowUp()
        (view as AutoCompleteTextView).showDropDown()
        false
    }

    this.setAdapter(adapter)

    //NOTE: its onItemClick for the suggestions, instead of onItemSelected as the spinner requires
    // https://stackoverflow.com/questions/9616812/how-to-add-listener-to-autocompletetextview-android
    this.onItemClickListener =
        AdapterView.OnItemClickListener { _, _, _, _ ->
            showArrowDown()
            parent.clearErrorMessage()
            afterItemSelected()
        }

    this.setOnDismissListener { showArrowDown() }

    this.filters = arrayOf<InputFilter>(object : InputFilter {
        //based on https://stackoverflow.com/questions/37152413/allowing-space-and-enter-key-in-android-keyboard
        override fun filter(
            charSequence: CharSequence,
            i: Int,
            i1: Int,
            spanned: Spanned,
            i2: Int,
            i3: Int
        ): CharSequence? {
            val ps = Pattern.compile("^[\n]+$")
            return if (charSequence != ""
                && !data.contains(charSequence)
                && !ps.matcher(charSequence).matches()
            ) {
                //not allowed
                ""
            } else null
        }
    })

}

fun TextInputLayout.setErrorMessage(errorMessage: String){
    this.error = errorMessage
}

fun TextInputLayout.clearErrorMessage(){
    this.error = null
}

fun EditText.getTextInputLayout(): TextInputLayout{
    return this.parent.parent as TextInputLayout
}