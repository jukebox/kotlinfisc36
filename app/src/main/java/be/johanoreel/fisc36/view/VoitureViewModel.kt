package be.johanoreel.fisc36.view

import androidx.lifecycle.ViewModel
import be.johanoreel.fisc36.voiture.Calcul
import be.johanoreel.fisc36.voiture.Moteur
import org.threeten.bp.LocalDate


class VoitureViewModel : ViewModel() {

    var exercice: Int? = null
    var valeurCatalogue: Double? = null
    var immatriculation: LocalDate? = null
    var moteur: Moteur? = null
    var emission: Int? = null
    var premierJour: LocalDate? = null
    var dernierJour: LocalDate? = null
    var calcul: Calcul? = null

}