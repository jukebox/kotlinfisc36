package be.johanoreel.fisc36.view

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.google.android.material.textfield.TextInputLayout
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

//https://stackoverflow.com/questions/14933330/datepicker-how-to-popup-datepicker-when-click-on-edittext
class DatePickerFragment(
    private val parent: TextInputLayout,
    private val defaultDate: LocalDate?,
    private val minDate: LocalDate?,
    private val maxDate: LocalDate,
    private val showYearPickerFirst: Boolean,
    private val formatter: DateTimeFormatter)
    : DialogFragment(), DatePickerDialog.OnDateSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val et = (parent.editText as EditText)

        val date : LocalDate = when {
            et.text.isNotEmpty() -> LocalDate.parse(et.text, formatter)
            defaultDate != null -> defaultDate
            else -> LocalDate.now()
        }

        val datePickerDialog = DatePickerDialog(requireContext(), this,
            date.year, date.monthValue-1, date.dayOfMonth)

        val datePicker = datePickerDialog.datePicker

        minDate?.let{
            val calMinDate = Calendar.getInstance()
            calMinDate.set(minDate.year, minDate.monthValue-1, minDate.dayOfMonth)
            datePicker.minDate = calMinDate.timeInMillis
        }

        val calMaxDate = Calendar.getInstance()
        calMaxDate.set(maxDate.year, maxDate.monthValue-1, maxDate.dayOfMonth)
        datePicker.maxDate = calMaxDate.timeInMillis

        //https://stackoverflow.com/questions/51356814/android-how-to-show-years-first-in-datepicker-when-onclick-the-button/51357081
        if(showYearPickerFirst){
            datePicker.touchables[0].performClick()
        }

        return datePickerDialog
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {
        (parent.editText as EditText).setText(LocalDate.of(year, month+1, day).format(formatter))
        parent.clearErrorMessage()
    }
}