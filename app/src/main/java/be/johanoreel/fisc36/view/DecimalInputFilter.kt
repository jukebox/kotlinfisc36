package be.johanoreel.fisc36.view

import android.text.InputFilter
import android.text.Spanned
import android.text.TextUtils
import java.util.regex.Pattern

// https://stackoverflow.com/questions/5357455/limit-decimal-places-in-android-edittext/24632346#24632346
class DecimalInputFilter(seperator: Char, digitsBeforeZero: Int, digitsAfterZero: Int) : InputFilter {

    private val defaultSeperator = '.'
    private val localeSeperator = seperator

    // Creates the pattern
    private val pattern: Pattern = Pattern.compile(
        "[0-9]{0," + digitsBeforeZero + "}+((" + localeSeperator +"[0-9]{0," + digitsAfterZero
                + "})?)||(\\.)?")


    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val replacement = source.subSequence(start, end).toString()
            // Workaround for Samsung's keyboard, replaces the dot by a comma, since
            // the comma key doesn't work on Samsung's keyboard
            .replace(defaultSeperator.toString(), localeSeperator.toString())
        val newVal = (dest.subSequence(0, dstart).toString() + replacement
                + dest.subSequence(dend, dest.length).toString())
        val matcher = pattern.matcher(newVal)
        if (matcher.matches()) return replacement
        return if (TextUtils.isEmpty(source)) dest.subSequence(dstart, dend) else ""
    }
}