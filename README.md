# Fisc36

Fisc36 is an Android app that makes it quick and easy to calculate company car related taxes in Belgium. In addition, you can view and share the detailed steps of the calculation. It is currently [available on the Play Store](https://play.google.com/store/apps/details?id=be.johanoreel.fisc36) (only in Belgium). 
