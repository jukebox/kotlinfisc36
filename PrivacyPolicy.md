# Privacy Policy

Fisc36, the Android app, does not collect, store or transmit any kind of personal data.

When Fisc36 is installed through Google Play, then Google Play might collect some personal data. For more information, please consult [Google's privacy policy](https://policies.google.com/privacy).
